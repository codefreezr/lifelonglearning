---
title: "Widescreen"
date: 2020-07-03T10:20:16+09:00
type: "gallery"
mode: "at-once" # at-once is a default value. one-by-one another mode.
description: "widescreen gallery"
image: images/feature2/Widescreen.png
meta_image: images/feature2/Widescreen.png
---
Widescreen Images for 5120x1440 Backgrounds.
