---
title: "100 Days of [No] Code <R4 />"
date: 2020-07-18T22:35:00
description: An introduction to NoCode, LowCode, NoLow, Visual Scripting, Pipeliner, RAD, MDA & more
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r4
  - nocode
  - lowcode
  - NoLow
  - RAD
  - MDA
series:
  - 100-doc-r4
categories:
  - 100DaysOfCode
image: images/r4/100DaysOfCodeR4-Logo.jpg
meta_image: images/r4/100DaysOfCodeR4-Logo.jpg
---

### 100 Days of [No]Code

<br/>

![](/lifelonglearning/images/r4/100DaysOfCodeR4-Logo.jpg)
 
 
#### NoCode
Today the fourth season of my #100DaysOfCode was started. And it is also my first round of **#100DaysOfNoCode**. NoCode? Wait, what is this about? How can an application consist of NoCode? Is there really no code left in the game?
 
Yes, of course, you need source code to create and operate a game, app or website. **NoCode** is about the maximum form of reuse. Prefabricated modules are combined together and result in a new solution. Code is only configured instead of, for example, repeatedly rewriting the same CRUD schema. Just win the race faster.
 
In this sense:
 
```Code is poetry, NoCode wizardry.```
 
Similar to the term serverless, since there are servers in the end, NoCode does not mean "There is no code", but "I use code that has already been written ". Sounds sustainable, doesn't it?
 
#### LowCode
A close cousin of NoCode is the **LowCode** approach. Gartner and Forrester offers solid arguments why it should be separated and not confused. From their point of view, to create marketsegments with new terms like LCDP or LCAP, you can understand that. The bridge to history is also maintained here: LowCode as a logical continuation of the **RAD** idea. I think it's OK and understandable, but not so important.
 
![](/lifelonglearning/images/r4/gartner-mq-2019.png) 
![](/lifelonglearning/images/r4/forrester-LCDP-2019-Q2.png)
 
 
The questions of familiarization, the learning curve are much more important and the path from idea to product. Or: Which skills do I have to bring? What skills can I learn quickly? What does the creation, generation and operation cost in term of money and time? How strong, helpful is the manufacturer, but above all the community?
 
#### Opensource
An objection to security should be discussed. What if my chic, quickly built NoCode app is completely riddled with third-party advertising? Or worse, user data is handed over directly to Facebook, Google & Co without knowledge? A consequent mitigation would be Open Source Software (OSS). Even if you would not be able to analyze sourceode, youd could consult services such as WhiteSource, Sonarqube and others. 
 
#### MDA
Another branch in this topic has grown out of the well known **MDA** approach. Flanked by the newer approach of "Event Driven Architecture", another area of ​​Visual Modeling, Programming & Scripting has emerged. “Executive UML” was far too complicated at the time. A breakthrough was only achieved through the metamorphosis from UML activity diagrams to BPMN 2.0, which, in addition to BPEL, first linked the execution with the model.
 
#### Visual Scripting. Coding & Modeling
In addition, new apps have been created, let's call them **"Visual Pipeliner"**. These have just gained fame and honor in event orchestration. how it can be found in IoT, AI, VR or game development. Examples are **Blueprint**, **NodeRed**, or the # **VUO** editor. One of the forefathers of this approach was **"Yahoo! Pipes"**. Unfortunately, that’s no longer the case.
 
![](/lifelonglearning/images/r4/pipe-blueprint.png)
![](/lifelonglearning/images/r4/pipe-vuo.png)
![](/lifelonglearning/images/r4/pipe-nodered.png)

 
 
#### NoLow
Perhaps we could summarize all of this in the term "Visual NoLow" or simply "NoLow". Pronounced NoLow means "NoCode is not LowCode". In this 4th season of 100 Days Of [No] Code, I will start to this world further, hopefully learn a lot of exciting things, and share what I have learned as soon as possible.
 
So we try to create and maintain different glossaries and possibly another “awesome collection” in order to persist knowledge and to offer further puzzle pieces for our global social bookmarking.
 
Regardless of the type of solution chosen, I will always give open source, as FOSS or COSS priority over closed source. By the way: did you know that the complete **UnrealEngine** source code is available on github? All you need is an Epic account and everyone can analyze, learn, expand themselves. One more reason why I now start with the UnrealEngine.
 
 
#### Overview
Here is my overview of the topics and areas
 
![](/lifelonglearning/images/r4/FirstMindMap.png)
 
Find [here](https://twitter.com/DetlefBurkhardt/status/1268306696574828544?S=20) another excellent overview of NoCode tools. During this R4 youd can find the most important tweets every day here: [Twitter Moment for R4](https://twitter.com/i/events/1284266516419051526?s=20)
 
 
 
### Glossary & Links
 
 
#### 100DaysOfCode
A hashtag out of the freecodecamp community.
[Homepage](https://www.100daysofcode.com/) 
 
#### 100DaysOfNoCode
A hashtag for learning NoCode tools & ideas
[Homepage](https://100daysofnocode.com)
 
 
#### AI
Artificial Intelligence. Make a decision based on what they have learned. 
[wikipedia, de](https://de.wikipedia.org/wiki/K%C3%BCnstliche_Intelligenz)
[wikipedia](https://en.wikipedia.org/wiki/Artificial_intelligence)
 
 
#### Blueprint
The visual programming environment, the pipeliner, within the **UnrealEngine**
[Getting Started](https://docs.unrealengine.com/en-US/Engine/Blueprints/GettingStarted/index.html)
 
 
#### BPEL
Business Process Execution Language. An XML-based markup language to describe and execute business processes.
[wikipedia, de](https://de.wikipedia.org/wiki/WS-Business_Process_Execution_Language)
[wikipedia](https://en.wikipedia.org/wiki/Business_Process_Execution_Language)
[List of BPEL-Engines](https: //en.wikipedia.org/wiki/List_of_BPEL_engines)
 
#### BPMN
Business Process Modeling Notations. A graphic markup syntax. From 2.0 onwards also contains an XML-based persistence in order to be able to execute the processes with so-called BPMN engines.
[wikipedia, de](https://de.wikipedia.org/wiki/Business_Process_Model_and_Notation)
[wikipedia](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation)
[List of BPMN 2.0 Engines, e](https://en.wikipedia.org/wiki/List_of_BPMN_2.0_engines)
[Comparism of Tools](https://en.wikipedia.org/wiki/Comparison_of_Business_Process_Model_and_Notation_modeling_tools)
 
#### CRUD
Short form for Create, Read, Update & Delete. Describes the basic operations in data processing.
[wikipedia, de](https://de.wikipedia.org/wiki/CRUD)
[wikipedia](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)
 
#### Forrester
A market research institute from the IT industry. 
[Homepage](https://go.forrester.com/)
[LCDP Free Download](https://go.caspio.com/forrester-wave-2019-low-code-platforms-business-developers-q2)
 
#### Gartner
A market research institute from the IT industry. 
[Homepage](https://www.gartner.com/en)
[LCAP MQ 2019/8](https://www.gartner.com/doc/reprints?id=1-1ODHN7R7&ct=190812&st=sb)
 
#### IoT
Internet of Things. Rough description of machine-to-machine communication via Internet protocols.
[wikipedia, de](https://de.wikipedia.org/wiki/Internet_der_Dinge)
[wikipedia](https://en.wikipedia.org/wiki/Internet_of_things)
 
#### MDA
Model Driven Architecture. Describes the approach of always developing and describing an architecture based on a model.
[wikipedia, de](https://de.wikipedia.org/wiki/Modellgetriebee_Architektur)
[wikipedia](https://en.wikipedia.org/wiki/Model-driven_architecture)
 
#### NodeRed
An FOSS tool Link events, especially in the IoT.
[Homepage](https://nodered.org/) 
 
#### OSS as FOSS or COSS
FOSS: Free & Open Source Software. 
COSS: Commercial OpenSource. More and more purely commercial software solutions disclose their code without being free.
OSI: Open Source Initiative. A NGO (Non Government Organization) to promote OSS.
[wikipedia, de](https://de.wikipedia.org/wiki/Open_Source)
[wikipedia](https://en.wikipedia.org/wiki/Open-source_model)
[OSI Homepage](https://opensource.org)
 
#### Pipes-digital
Another successor to Yahoo! Pipes. With a Community Edition (CE) in OpenSource.
[github](https://github.com/pipes-digital/pipes)
[homepage](https://www.pipes.digital/)
 
#### RAD
Rapid Application Development. Describes a procedure for reusing a solution as quickly as possible.
[wikipedia, de](https://de.wikipedia.org/wiki/Rapid_Application_Development)
[wikipedia](https://en.wikipedia.org/wiki/Rapid_application_development)
 
 
#### SCA
Static Code Analysis. With the help of SCA errors, problems, technical debts, security gaps can be discovered.
[wikipedia, de](https://de.wikipedia.org/wiki/Statik_Code-Analyse)
[wikipedia](https://en.wikipedia.org/wiki/Static_program_analysis)
 
#### Sonarqube
A widely used tool in the area of ​​the SCA,
[Homepage](https://www.sonarqube.org/) 
 
#### UML
Unified Modeling Language. A collection of ~13 diagrams to graphically document classes, components, deployments, processes and more. It is managed by the OMG (Object Managing Group).
[OMG UML Vendors](https://www.omg.org/uml-directory/) 
[wikipedia, de](https://de.wikipedia.org/wiki/Unified_Modeling_Language)
[wikipedia](https: // en .wikipedia.org / wiki / Unified_Modeling_Language)
 
#### Unreal
Synonym for the Unreal Engine. A real-time 3D rendering and ray tracing platform. Used for many AAA computer games but also for some VFX sequences in film production, for example at HBO or ILM.
[Homepage](https://www.unrealengine.com) 
 
#### VR
Virtual Reality. Describes an immersive 3D experience usually with the help of special glasses, gloves and controllers.
[wikipedia, de](https://de.wikipedia.org/wiki/Virtuelle_Realit%C3%A4t)
[wikipedia](https://en.wikipedia.org/wiki/Virtual_reality)
 
#### VUO
VUO is a tool for mixing and mashup real-time multimedia content.
[Homepage](https://vuo.org/what-is-vuo)
 
#### WhiteSource
A SCA tool that uses different databases to recognize whether there have already been security problems with an OSS modules and whether or how they have been solved.
[Homepage](https://www.whitesourcesoftware.com/) 
 
#### Yahoo Pipes
One of the first web-based “NoCode” solutions to link different events and mashups rss. Unfortunately, it no longer exists. **NodeRed** or **pipe-digital** comes closest to that.
[wikipedia](https://en.wikipedia.org/wiki/Yahoo!_Pipes)
 
 
 
 
 
 
 

