---
title: "R3:D21 - raspy"
date: 2020-03-01T22:35:51+09:00
description: Raspberry PI - where Python becomes hardware.
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - raspberry pi
  - hw
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/raspy.png
meta_image: images/r3/raspy.png
---

#### Raspberry PI 4b

<br/>

![](/lifelonglearning/images/r3/raspy.png)

My first Raspi arrived yesterday. After unpacking it I had to realize that it felt like soldering the gameboy-homebrew roms 20 years ago:

- first format the micro-sd: "format /FS:FAT32 F:"
- download an image, for e.g. raspbian
- burn the image on the micro sd
- plugin into the raspi
- boot
- strike!

Stay tuned, this was just the begining ... tbc

<br/><br/>

#### Some Links:

- [homepage](https://www.raspberrypi.org/)
- [awesome](https://awesome-rpi.netlify.com/)
- [cases](https://www.electromaker.io/blog/article/best-raspberry-pi-4-cases-top-raspberry-pi-4-compatible-cases)
