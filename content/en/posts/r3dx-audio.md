---
title: "R3:Dx - Audio"
date: 2020-02-16T19:23:51+09:00
description: Python for Soundengineer
draft: true
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - klang
  - music
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/PyPi-Logo-And-Walleve.png
meta_image: images/r3/PyPi-Logo-And-Walleve.png
---

![](/lifelonglearning/images/r3/PyPi-Logo-And-Walleve.png)

echnest
remix
soundcloud
drummachine
youtube as soundsource
spotify
