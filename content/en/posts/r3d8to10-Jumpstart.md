---
title: "R3:D8to10 - Pythonic Jumpstart"
date: 2020-02-19T23:55:51+09:00
description: Pythonic Jumpstart for Developers
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - jumpstart
  - pythonic
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/Jumpstart.png
meta_image: images/r3/Jumpstart.png
---

#### Pythonic Jumpstart

<br/>

![](/lifelonglearning/images/r3/Jumpstart.png)

If you are a coder, perhaps a polyglot one, than you know a lot of basic concepts. You need only a collection of examples then you're in. Today I found a nice jumpstart, also in German. It's ugly, old-school HTML, but it has all the stuff I need to understand the basics.

This was the starting point to collect more jumpstart, quick-guides or introductions. On this path I got stuck with this shining term: pythonic. See what Raymond Hettinger thinks about pythonic-thinking:

[![IMAGE ALT TEXT HERE](/lifelonglearning/images/r3/pythonic.png)](https://www.youtube.com/watch?v=OSGv2VnC0go)

<br/>

#### List of smart Jumpstarts (de):

1. [Das Python Tutorial](https://py-tutorial-de.readthedocs.io/de/python-3.3/)
1. [Python macht Spaß](https://www.thomas-guettler.de/vortraege/python/einfuehrung.html)
1. [Python lernen in deutsch auf Youtube](https://www.youtube.com/playlist?list=PLNmsVeXQZj7q0ao69AIogD94oBgp3E9Zs)
1. [Einführung in Python 3](https://www.python-kurs.eu/python3_kurs.php)
1. [Python-Wiki German](https://wiki.python.org/moin/GermanLanguage)

<br/>

#### List of smart Jumpstarts (e):

1. [learningpython.org](https://www.learnpython.org/)
1. [automate the boring stuff](https://inventwithpython.com/)
1. [Python for Groovies](https://medium.com/@yash_agarwal2/learning-python-as-a-groovy-developer-c8b8179aa3f8)
1. [W3School](https://www.w3schools.com/python/default.asp)
1. [freecodecamp](https://guide.freecodecamp.org/python)
1. [The Python Tutorial](https://docs.python.org/3/tutorial/)
1. [Introduction to Python 3](https://pythonprogramming.net/introduction-learn-python-3-tutorials/)

#### Free Courses (e)

1. [dataquest.io fundamentals](https://www.dataquest.io/course/python-for-data-science-fundamentals/)
1. []()

<br/>

#### Another List of Learning Resources (e):

1. [Python for NonProgrammers](https://wiki.python.org/moin/BeginnersGuide/NonProgrammers)
1. [Python for Programmers](https://wiki.python.org/moin/BeginnersGuide/Programmers)
1. [Free Python3 Tutorials and Courses](https://hackr.io/tutorials/learn-python?sort=upvotes&type_tags%5B%5D=1&sub_topics%5B%5D=171&languages%5B%5D=en)
