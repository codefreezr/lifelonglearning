---
title: "R3:D102 - JSONSchema"
date: 2020-06-02T00:45:51+09:00
description: Give JSON a Schema.
draft: true
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - javascript
  - json
  - schema
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/jsonschema.jpg
meta_image: images/r3/jsonschema.jpg
---

### JSONSchema

<br/>

![](/lifelonglearning/images/r3/jsonschema.jpg)

If you compare JSON/Yaml with for e.g. XML there are two things missing: Semantic and Syntax-Declarations. While JSON did not have xml-like tags, semantic is not that easy possible. In the XML World we have XSD for Syntax Sefinitions. Could there be something like this for JSON too? 

## JSONSchema to the rescue

## Understanding JSONSchema

## Generating Schema

## Tools Online Editor
Altova


