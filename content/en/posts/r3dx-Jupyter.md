---
title: "R3:D13 - Jupyter"
date: 2020-02-18T19:23:51+09:00
description: More fun to compyte with Jupyter Notebooks
draft: true
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - groovy
  - jython
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/PyPi-Logo-And-Walleve.png
meta_image: images/r3/PyPi-Logo-And-Walleve.png
---

Online Jupyter-Notebooks as a Service (JaaS) are the beste environment to learn coding, especially if you have a polyglot thinking.

As a polyglot coder two very important streams attracted.

Als Polyglot Entwickler haben mich zwei Entwicklungen in den letzen Jahren besonders beindruckt: FaaS und Jupyter Notebooks.
Mit Jupyter Notebooks kannst Du Markdown und Executables direkt nebeneinander speichern. Das ganze nicht nur in Python sondern auch in R, Julia oder auch Javascript und viele Sprachen mehr.

Von daher will ich mich die nächsten beiden Wochen einmal intensiver mit Jupyter Notebook, der Jupyter Foundation und einigen Implementierungen, wie

PAWS @Wikitech
Plain Jupyter Notebook
ipynb inside VSCode
[notebooks.ai](https://notebooks.ai/CodeFreezR)
[mybinder](https://mybinder.org/)
[Google Colab](https://colab.research.google.com/notebooks/intro.ipynb#scrollTo=GJBs_flRovLc)

https://mybinder.readthedocs.io/en/latest/

galaxy offers jupyter, rstudio & red.noise pipelines
rstudio & jupyter
AWS Sagemaker

![](/lifelonglearning/images/r3/PyPi-Logo-And-Walleve.png)

Today I made my first Jupyter Notebook.

Here some starter Guides:
(https://www.analyticsvidhya.com/blog/2018/05/starters-guide-jupyter-notebook/)
(https://medium.com/codingthesmartway-com-blog/getting-started-with-jupyter-notebook-for-python-4e7082bd5d46)
(https://github.com/Noura/hello-jupyter)

Examples:
(https://github.com/Tessellate-Imaging/monk_v1/tree/master/study_roadmap/image_processing_basics)
(https://github.com/jupyter/jupyter/wiki/A-gallery-of-interesting-Jupyter-Notebooks)
