---
title: "R3:D17 - wclock"
date: 2020-02-26T22:54:51+09:00
description: Simple CLI for worldtime, cw and local timezone
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - timedelta
  - worldtime
  - timezone
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/pywclock.png
meta_image: images/r3/pywclock.png
---

#### wclock

<br/>

![](/lifelonglearning/images/r3/pywclock.png)

Today I've learned about time calculations, timezones and more. A small py script gives me my local timezone, the day of the week, the calenderweek and the most important timezones.

```python
#!/usr/bin/env python

import datetime as dt

wtag = [
    "Montag",
    "Dienstag",
    "Mittwoch",
    "Donnerstag",
    "Freitag",
    "Samstag",
    "Sonntag"
]


city = {
    "Los Angeles": {"tz": "US/Pacific (PST)", "delta": -9},
    "New York": {"tz": "US/Eastern (EST)", "delta": -6},
    "Sao Paulo": {"tz": "America/Sao_Paulo (BRT)", "delta": -4},
    "London  ": {"tz": "Europe/London (GMT/BST)", "delta": -1},
    "Berlin  ": {"tz": "Europe/Berlin (CET/CEST)", "delta": 0},
    "Moscow  ": {"tz": "Europe/Moscow (MSK)", "delta": 2},
    "New Dehli  ": {"tz": "Indian Standard Time (IST)", "delta": 4},
    "Beijing  ": {"tz": "China Standard Time (CST)", "delta": 7},
    "Tokyo  ": {"tz": "Japan Standard Time (JST)", "delta": 8},
    "Sydney  ": {"tz": "Australia/Sydney (AEST/AEDT)", "delta": 9},
    "Auckland  ": {"tz": "Pacific/Auckland (NZST/NZDT)", "delta": 11},
}

LOCAL_TIMEZONE = dt.datetime.now(
    dt.timezone(dt.timedelta(0))).astimezone().tzinfo

a = dt.datetime.now()
wd = dt.datetime.today().weekday()
cw = a.isocalendar()[1]


print(str(LOCAL_TIMEZONE) + ": " + str(a) + ", " +
      wtag[wd] + " in der Kalenderwoche " + str(cw))
print("---")

for key, value in city.items():
    print(key, '\t{:%H:%M:%S}'.format(
        a + dt.timedelta(hours=value["delta"])), '\t', value["tz"])

```
