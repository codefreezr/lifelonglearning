---
title: "R3:D4 - Py And Code"
date: 2020-02-13T19:23:51+09:00
description: Using Visual Studio Code for coding in Python
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - vscode
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/PyPlusCode-transparenz.png
meta_image: images/r3/PyPlusCode.png
---

![](/lifelonglearning/images/r3/PyPlusCode-transparenz.png)

I'm surprised at how far Python support goes within vscode. The official microsoft python plugin itself is worth to switch to vscode just for your python coding.

The top feature list:

- Linting
- Debugging (multi-threaded, remote)
- Intellisense
- Jupyter Notebooks
- code formatting, refactoring, unit tests, snippets and more

I am most curious about the Jupyter Notebook Support, also because the official Jupyter Notebook Plugin will be continued in the Microsoft "Python for VS-Code" plugin.

This micro-screencast show directly what you can expect:

![](/lifelonglearning/images/r3/python-editing.gif)

<br/>
Here the list of plugins I have installed at first:

- [Python for VS Code](https://github.com/Microsoft/vscode-python)
- [python snippets](https://github.com/ylcnfrht/vscode-python-snippet-pack)
- [robot Framework](https://github.com/tomi/vscode-rf-language-server)
- autopep8-1.5
- pycodestyle-2.5.0

<br/>
Here some vsCode Links:

- [Wikipedia](https://de.wikipedia.org/wiki/Visual_Studio_Code)
- [Erich Gamma](https://de.wikipedia.org/wiki/Erich_Gamma)
- [become the most popular code editor](https://jaxenter.com/we-want-vs-code-to-become-the-most-popular-code-editor-interview-with-erich-gamma-127670.html)
- [Awesome VsCode](https://viatsko.github.io/awesome-vscode/)

<br/>
Here some pyhtonistas stuff direct for doing py in code:

1. [Tutorial](https://code.visualstudio.com/docs/python/python-tutorial)
1. [Editing Code](https://code.visualstudio.com/docs/python/editing)
1. [Linting](https://code.visualstudio.com/docs/python/linting)
1. [Debugging](https://code.visualstudio.com/docs/python/debugging)
1. [Jupyter Notebook Support](https://code.visualstudio.com/docs/python/jupyter-support)
1. [Python Interactive](https://code.visualstudio.com/docs/python/jupyter-support-py)
1. [Environments](https://code.visualstudio.com/docs/python/environments)
1. [Testing](https://code.visualstudio.com/docs/python/testing)
1. [Django Tutorial](https://code.visualstudio.com/docs/python/tutorial-django)
1. [Flask Tutorial](https://code.visualstudio.com/docs/python/tutorial-flask)
1. [Create containers](https://code.visualstudio.com/docs/python/tutorial-create-containers)
1. [Python on Azure](https://code.visualstudio.com/docs/python/python-on-azure)
1. [Settings Reference](https://code.visualstudio.com/docs/python/settings-reference)
