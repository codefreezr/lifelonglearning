---
title: "R3:Dx - Robot Framework"
date: 2020-02-17T19:23:51+09:00
description: Robot Framework for RPA and Testautomation
draft: true
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - robotframework
  - module
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/PyPi-Logo-And-Walleve.png
meta_image: images/r3/PyPi-Logo-And-Walleve.png
---

# Robot Framework

![](/lifelonglearning/images/r3/PyPi-Logo-And-Walleve.png)

Robot peep tuut.

Steps:

- pip install robotframework
- pip install docutils
- https://github.com/robotframework/QuickStartGuide/blob/master/QuickStart.rst
- https://www.tutorialspoint.com/robot_framework/index.htm
- https://blog.codecentric.de/en/2012/04/robot-framework-tutorial-a-complete-example/
- http://www.robotframeworktutorial.com/
