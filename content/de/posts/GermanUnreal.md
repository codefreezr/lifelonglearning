---
title: "Unreal in deutsch"
date: 2020-07-26T23:35:51+09:00
description: Unreal in deutsch. Einige Übersetzungen und Sammlungen von deutschsprachigen Ressourcen
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - unrealenginge
series:
  - gamedev
categories:
  - gamedev
image: images/r4/GermanUnreal.png
meta_image: images/r4/GermanUnreal.png
---

### Unreal Engine in deutsch

<br/>

![](/lifelonglearning/images/r4/GermanUnreal.png)


Warum unreal in deutsch? Die Game-Engine ist in english, die aktuellesten Tutorials und Guides sind in englisch. Selbst die offiziellen Lernpfade sind alle in english. Meistens ist das ok, aber im Detail ist es manchmal schwierig den schnell sprechenden Lehrern mit Akzent zu folgen. 

Auch sind die meistens Details in der Unreal-Oberfläche in englisch erklärt, aber nicht selten nur mit Vorkenntnissen zu verstehen. Auch fehlen an einigen Stellen wichtige "Cross-References" um z.b. Überblicke über alle 500++ Blueprint Klassen, Komponenten, Events etc. zu bekommen. 

Dann wären das in etwa die Ziele dieser Seite:
+ deutschsprachiger Ressourcen
+ Mehrsprachiges Glossar und Abkürzungsverzeichnis
+ Teilübersetzung aus dem unreal editor


Eventuell entsteht daraus noch das ein oder andere github/gitlab Repo, aber lieber mal was anfangen, als drüber zu reden was sein könnte.

Das soll jetzt nicht bedeutet, das ihr alles auf deutsch umstellen sollt, weil sonst wird es schwierig zu bestimmten Themen was über google zu finden.


### Deutschsprachige Unreal-Ressourcen

#### Videos (tutorials)
* [Spielstand speichern und laden - UnrealGaimeDev - 2016-07-27](https://www.youtube.com/watch?v=KbpcgcdboxM)
* [Unreal Engine 4 Tutorial - Morpheus - 2017-01-03](https://www.youtube.com/watch?v=U7Cs3gkelAU&list=PLNmsVeXQZj7olLCliQ05e6hvEOl6sbBgv)
* [Über 85 Unreal Engine Tutorials - Der Sky](https://www.youtube.com/playlist?list=PLjl08Kt9zoxT9yUAF_17ppFfvEY-n-2FS)
* [UNREAL ENGINE 4 | MAIN MENU Erstellen - GermanUnreal TUT - 2017-08-28](https://www.youtube.com/watch?v=IvLFjpmThSU)
* [Blueprint Tutorial | UE4 | Deutsch - MrZelektronz - 2017-08-21](https://www.youtube.com/watch?v=gZdog00-YgY)
* [Fast & Dirty Unreal Engine 4 Tutorial - Timessv - 2019-07-06](https://www.youtube.com/watch?v=XEi2mVbLS7s)
* [Unreal Engine 4 Tutorial - NOBODY - 2014-03-22](https://www.youtube.com/watch?v=cokU5ANcILk)

#### Videos (Game Engines)
* [Unreal, Unity oder doch Godot? Welche Engine? - DevPlay - 2020-04-14](https://www.youtube.com/watch?v=eCdrJqHFqaw)
* [Welche Game-Engine sollte man nutzen? - BumBum - 2018-03-07](https://www.youtube.com/watch?v=JAFgn20zVBE)


#### Web, News & Feeds
* [Digital Production #unreal-engine](https://www.digitalproduction.com/tag/unreal-engine/)
* [PlayFab für Unreal - 2020-03-10](https://docs.microsoft.com/de-de/gaming/playfab/sdks/unreal/quickstart)
* [German Developer, Unreal Forum](https://forums.unrealengine.com/international/europe/471-german-developers?view=stream)
* [UnrealEngine4DE - Forum](https://unrealengine4.de/)
* [Wikipedia](https://de.wikipedia.org/wiki/Unreal_Engine)
* [Unreal Enginge News Reader](https://news.feed-reader.net/58875-unreal-engine.html)

#### Bücher & Artikel
* [Spiele entwickeln mit Unreal Engine 4, 2nd - Hanser - 2017-11-01](https://www.hanser-fachbuch.de/buch/Spiele+entwickeln+mit+Unreal+Engine+4/9783446452909)
* [Spiele programmieren mit der Unreal Engine für Kids - MITP - 2017-01-13](https://www.mitp.de/Unsere-Autoren/Hans-Georg-Schumann/Spiele-programmieren-mit-der-Unreal-Engine-fuer-Kids.html)
* [Unreal 4 für Einsteiger - CHIP - 2019-11-04](https://praxistipps.chip.de/unreal-engine-4-tutorial-fuer-einsteiger-alle-details_94515)

#### Deutschsprachige Unrealisten
* [Der Sky - Youtube](https://www.youtube.com/channel/UC2Ey1K5b4NjBaVRYEYCEGiQ)
* [Der Sky - Twitter](https://twitter.com/der_skyyy)

* [N0B0DY / Jonas Richartz - Youtube](https://www.youtube.com/channel/UCKbX1w3t3E_-ypBPhtz4upA)
* [N0B0DY / Jonas Richartz - Twitter](https://twitter.com/jonas_richartz?lang=de)
* [N0B0DY / Jonas Richartz - Web](http://www.jonasrichartz.com/)

* [Morpheus - Youtube](https://www.youtube.com/channel/UCLGY6_j7kZfA1dmmjR1J_7w)
* [Morpheus - Discord](https://discord.gg/BnYZ8XS)
* [Morpheus - Web](https://the-morpheus.de/)
* [Morpheus - Twitter](https://twitter.com/TheMorpheusTuts)








### Teilübersetzung aus dem unreal editor

#### Blueprint Klassen
1. Actor
1. Spawn
1. 

#### Blueprint Komponenten
1. Komponenten 1
1. Komponenten 2






### Glossar & Links

Ein erster Anfang:

#### 100DaysOfCode
Eine Hashtag aus dem Freecode-Umfeld.
[Homepage](https://www.100daysofcode.com/) 

#### 100DaysOfNoCode
Ein Hashtag für das Lernen von NoCode-Werkzeugen & -Ideen
[Homepage](https://100daysofnocode.com)


#### AI
Artificial Intelligence, dt. Künstliche Intelligenz. Entscheidung auf Grundlage von Gelerntem treffen. 
[wikipedia, de](https://de.wikipedia.org/wiki/K%C3%BCnstliche_Intelligenz)
[wikipedia](https://en.wikipedia.org/wiki/Artificial_intelligence)


#### Blueprint
Die visuelle Programmierumgebung, der Pipeliner, innerhalb der Unreal Engine
[Getting Started](https://docs.unrealengine.com/en-US/Engine/Blueprints/GettingStarted/index.html)


#### BPEL
Business Process Execution Language. Eine auf XML basierende Auszeichnungssprache um Business Prozesse zu beschreiben und ausführen zu können.
[wikipedia, de](https://de.wikipedia.org/wiki/WS-Business_Process_Execution_Language)
[wikipedia](https://en.wikipedia.org/wiki/Business_Process_Execution_Language)
[List of BPEL-Engines](https://en.wikipedia.org/wiki/List_of_BPEL_engines)

#### BPMN
Business Process Modelling Notations. Eine grafische Auszeichnungs Syntax. Enthält ab 2.0 auch eine XML-basierte Persistenz um die Prozesse mit sog. BPMN-Engines ausführen lassen zu können.
[wikipedia, de](https://de.wikipedia.org/wiki/Business_Process_Model_and_Notation)
[wikipedia](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation)
[List of BPMN 2.0 Engines, e](https://en.wikipedia.org/wiki/List_of_BPMN_2.0_engines)
[Comparism of Tools](https://en.wikipedia.org/wiki/Comparison_of_Business_Process_Model_and_Notation_modeling_tools)

#### CRUD
Kurzform für Create, Read, Update & Delete. Beschreibt die Basis-Operationen in der Datenverarbeitung.
[wikipedia, de](https://de.wikipedia.org/wiki/CRUD)
[wikipedia](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)

#### Forrester
Ein Marktforschungsinstitut aus der IT-Branche. 
[Homepage](https://go.forrester.com/)
[LCDP Free Download](https://go.caspio.com/forrester-wave-2019-low-code-platforms-business-developers-q2)

#### Gartner
Ein Marktforschungsinstitut aus der IT-Branche. 
[Homepage](https://www.gartner.com/en)
[LCAP MQ 2019/8](https://www.gartner.com/doc/reprints?id=1-1ODHN7R7&ct=190812&st=sb)

#### IoT
Internet of Things. Grobe Umschreibung von Maschine-zu-Maschine Kommunikation via Internet-Protokolle.
[wikipedia, de](https://de.wikipedia.org/wiki/Internet_der_Dinge)
[wikipedia](https://en.wikipedia.org/wiki/Internet_of_things)

#### MDA
Model Driven Architecture. Beschreibt den Ansatz, eine Architektur immer auf Basis eines Modells zu entwickeln und zu beschreiben.
[wikipedia, de](https://de.wikipedia.org/wiki/Modellgetriebene_Architektur)
[wikipedia](https://en.wikipedia.org/wiki/Model-driven_architecture)

#### NodeRed
Ein Opensource-Tool um Events, vor allem in der IoT, miteinander zu verknüpfen.
[Homepage](https://nodered.org/) 

#### OSS as FOSS or COSS
FOSS: Free & Open Source. Freie und Quelloffene Software.
COSS: OpenSource, hier Commercial OpenSource. Immer mehr rein kommerzielle Software-Lösungen legen ihren Code offen, ohne dabei kostenlos zu sein.
OSI: OpenSource Initiative. A NGO (Non Government Organisation)  to promote OSS.
[wikipedia, de](https://de.wikipedia.org/wiki/Open_Source)
[wikipedia](https://en.wikipedia.org/wiki/Open-source_model)
[OSI Homepage](https://opensource.org/)

#### Pipes-digital
Ein weiterer Nachfolger von Yahoo! Pipes. Mit einer Community Edition (CE) in OpenSource.
[github](https://github.com/pipes-digital/pipes)
[homepage](https://www.pipes.digital/)

#### RAD
Rapid Application Development. Beschreibt ein Vorgehen durch Wiederverwendung möglichst schnell eine Lösung fertig zu stellen.
[wikipedia, de](https://de.wikipedia.org/wiki/Rapid_Application_Development)
[wikipedia](https://en.wikipedia.org/wiki/Rapid_application_development)


#### SCA
Statische Code Analyse. Mit Hilfe SCA können Fehler, Probleme, technische Schulden, Sicherheitslücken entdeckt werden.
[wikipedia, de](https://de.wikipedia.org/wiki/Statische_Code-Analyse)
[wikipedia](https://en.wikipedia.org/wiki/Static_program_analysis)

#### Sonarqube
Ein weit verbreitetes Tool aus dem Bereich der SCA,
[Homepage](https://www.sonarqube.org/) 

#### UML
Unified Modelling Language. Eine Sammlung von ~13 Diagrammen um Klassen, Komponenten, Deployments, Abläufe und mehr grafisch zu dokumentieren.Wird von der OMG (Object Managing Group) geführt.
[OMG UML Vendors](https://www.omg.org/uml-directory/) 
[wikipedia, de](https://de.wikipedia.org/wiki/Unified_Modeling_Language)
[wikipedia](https://en.wikipedia.org/wiki/Unified_Modeling_Language)

#### Unreal
Synonym für die Unreal Engine. Eine Echtzeit 3D- Rendering und Raytracing Plattform. Wird für viele AAA Computerspiele aber auch für einige VFX-Sequenzen in Filmproduktion u.a. bei HBO oder ILM benutzt.
[Homepage](https://www.unrealengine.com) 

#### VR
Virtual Reality. Beschreibt ein immersives 3D Erlebnis in der Regel mit Hilfe spezieller Brillen und Gloves.
[wikipedia, de](https://de.wikipedia.org/wiki/Virtuelle_Realit%C3%A4t)
[wikipedia](https://en.wikipedia.org/wiki/Virtual_reality)

#### VUO
VUO, vor allem aber der VUO-Editor ist ein Tool für das Mixen von Echtzeit-Multimedia Inhalten.
[Homepage](https://vuo.org/what-is-vuo)

#### WhiteSource
Ein SCA Tool, welches mithilfe verschiedener Datenbank erkennt, ob es mit OSS-Modulen bereits schon Sicherheitsproblem gab und ob bzw. wie diese gelöst wurden.
[Homepage](https://www.whitesourcesoftware.com/) 

#### Yahoo Pipes
Einer der ersten webbasierten “NoCode”-Lösungen um verschiedene Events miteinander zu verknüpfen. Gibt es so leider nicht mehr. NodeRed kommt dem am nächsten.
[wikipedia](https://en.wikipedia.org/wiki/Yahoo!_Pipes)





