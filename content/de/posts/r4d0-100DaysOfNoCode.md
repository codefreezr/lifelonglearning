---
title: "100 Days of [No]Code <R4/>"
date: 2020-07-18T22:35:51+09:00
description: Eine Einführung in NoCode, LowCode, NoLow, Visual Scripting, Pipeliner, RAD, MDA & more
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r4
  - nocode
  - lowcode
  - NoLow
  - RAD
  - MDA
series:
  - 100-doc-r4
categories:
  - 100DaysOfCode
image: images/r4/100DaysOfCodeR4-Logo.jpg
meta_image: images/r4/100DaysOfCodeR4-Logo.jpg
---

### 100 Days of [No]Code

<br/>

![](/lifelonglearning/images/r4/100DaysOfCodeR4-Logo.jpg)


#### NoCode
Heute fange ich mit einer vierten Staffel von 100DaysOfCode an. Und es ist zugleich meine erste Runde von **100DaysOfNoCode**. NoCode? Was soll das denn jetzt? Wie kann denn eine Applikation aus NoCode bestehen? Ist da wirklich kein Code mehr im Spiel?

Ja doch, natürlich braucht es Sourcecode um ein Game, eine App oder Website zu erstellen und zu betreiben. Bei NoCode geht es um die Maximalform der Wiederverwendung. Vorgefertigte Module werden zusammen kombiniert und ergeben ein neues Ganzes. Code wird nur noch konfiguriert anstatt zum Beispiel immer wieder das selbe CRUD-Schema neu zu schreiben. Einfach schneller zum Ziel.

In diesem Sinne:

```Code is poetry, NoCode wizardry.```

Ähnlich wie beim Begriff Serverless, da gibts ja im End doch auch Server, bedeutet NoCode nicht "Da gibt es keinen Code", sondern "Ich verwende bereits geschriebenen Code". Klingt doch nachhaltig, oder?

#### LowCode
Ein naher Verwandter von NoCode ist der LowCode Ansatz. Gartner und Forrester erheben stichhaltige Argumente, warum man das unbedingt voneinander trennen und nicht verwechseln sollte. Aus ihrer Sicht heraus, immer wieder mit neuen Begriffen, wie LCDP oder LCAP Märkte zu segmentieren, kann man das nachvollziehen. Auch ist hier die Brücke zur Geschichte gewahrt, LowCode als logische Fortsetzung der RAD (Rapid Application Development) Idee. Ich find's OK und nachvollziehbar, aber nicht ganz so wichtig.

![](/lifelonglearning/images/r4/gartner-mq-2019.png) 
![](/lifelonglearning/images/r4/forrester-LCDP-2019-Q2.png)


Viel wichtiger sind die Fragen des Einarbeitungsaufwands, der Lernkurve und des Wegs von Idee zum Produkt. Oder: Welche Skills muss ich mitbringen? Welche Skills kann ich schnell lernen? Was kostet die Erstellung, die Generierung und der Betrieb? Wie stark, hilfreich entgegenkommend ist der Hersteller, aber vor allem die Community?

#### Opensource
Ein Einwand zum Thema Sicherheit sollte besprochen werden. Was ist denn, wenn meine schick, schnell gebaute NoCode App, komplett mit Fremdwerbung durchseucht ist? Oder schlimmer, Nutzer-Daten ohne Kenntnis direkt an Facebook, Google & Co ausgehändigt werden? Eine konsequente Lösung wäre hier OpenSource-Software (OSS). Auch wenn ich selber nicht in der Lage wäre Sourceode zu analysieren, so könnte ich hierzu Services, wie WhiteSource, Sonarqube u.a. zu Rate ziehen. 

#### MDA
Ein anderer Zweig in diesem Thema ist aus dem MDA (Model Driven Architecture) Ansatz heraus erwachsen. Flankiert vom neueren Ansatz der "Event Driven Architecture" ist ein weiterer Bereich des Visual Modelling und Visual Programing & Scripting entstanden. “Executive UML” war damals noch viel zu kompliziert. Erst durch die Metamorphose von UML-Activity-Diagrammen hin zur BPMN 2.0, die neben BPEL erstmal die Ausführung mit dem Modell verbunden hat, ist ein Durchbruch gelungen.

#### Visual Scripting. Coding & Modelling
Daneben sind neue Apps enstanden, nennen wir sie, Visual Pipeliner. Diese sind gerade in der Event-Orchestrierung zu Ruhm und Ehren gelangt. Event-Orchestrierungen finden sich in IoT, AI, VR oder der Spiele-Entwicklung. Beispiele sind hier #Blueprint, #NodeRed, oder der #VUO-Editor. Einer der Urväter dieses Ansatzes war "Yahoo! Pipes". Gibt’s so leider nicht mehr.

![](/lifelonglearning/images/r4/pipe-blueprint.png)
![](/lifelonglearning/images/r4/pipe-vuo.png)
![](/lifelonglearning/images/r4/pipe-nodered.png)


#### NoLow
All das fasse ich in dem Begriff "Visual NoLow" oder einfach "NoLow" zusammen. Ausgeprochen bedeutet NoLow dann "NoCode is not LowCode". In meiner 4ten Staffel von 100DaysOf[No]Code werde ich anfangen, diese Welt weiter zu entdecken, hoffentlich viele spannende Dinge zu lernen, und so gut es geht das Gelernte gleich wieder zu teilen.

Dabei versuche ich verschiedene Glossare und evt. eine weitere “awesome collection” zu erstellen und zu pflegen, um Wissen zu persistieren und weitere Puzzlestücke für ein globales Social Bookmarking anzubieten.

Unabhängig von der Art der gewählten Lösung werde ich OpenSource, als FOSS oder COSS immer erstmal den Vorrang vor Closed-Source geben. Apropos: Wusstet ihr, dass der komplette Unreal Sourcecode auf github zur Verfügung steht? Es braucht nur einen Epic-Account und schon kann jeder selber analysieren, lernen, erweitern. Ein Grund mehr warum ich nun erstmal mit der unreal engine anfange.


#### Übersicht
Hier meine Übersicht der Themen und Bereiche:

![](/lifelonglearning/images/r4/FirstMindMap.png)

Über eine andere hervorragende Übersicht von NoCode-Tools habe ich [hier](https://twitter.com/DetlefBurkhardt/status/1268306696574828544?s=20) getwittert. Während dem R4 halte ich die wichtigeste tweets täglich hier fest: [Twitter Moment for R4](https://twitter.com/i/events/1284266516419051526?s=20)



### Glossar & Links


#### 100DaysOfCode
Eine Hashtag aus dem Freecode-Umfeld.
[Homepage](https://www.100daysofcode.com/) 

#### 100DaysOfNoCode
Ein Hashtag für das Lernen von NoCode-Werkzeugen & -Ideen
[Homepage](https://100daysofnocode.com)


#### AI
Artificial Intelligence, dt. Künstliche Intelligenz. Entscheidung auf Grundlage von Gelerntem treffen. 
[wikipedia, de](https://de.wikipedia.org/wiki/K%C3%BCnstliche_Intelligenz)
[wikipedia](https://en.wikipedia.org/wiki/Artificial_intelligence)


#### Blueprint
Die visuelle Programmierumgebung, der Pipeliner, innerhalb der Unreal Engine
[Getting Started](https://docs.unrealengine.com/en-US/Engine/Blueprints/GettingStarted/index.html)


#### BPEL
Business Process Execution Language. Eine auf XML basierende Auszeichnungssprache um Business Prozesse zu beschreiben und ausführen zu können.
[wikipedia, de](https://de.wikipedia.org/wiki/WS-Business_Process_Execution_Language)
[wikipedia](https://en.wikipedia.org/wiki/Business_Process_Execution_Language)
[List of BPEL-Engines](https://en.wikipedia.org/wiki/List_of_BPEL_engines)

#### BPMN
Business Process Modelling Notations. Eine grafische Auszeichnungs Syntax. Enthält ab 2.0 auch eine XML-basierte Persistenz um die Prozesse mit sog. BPMN-Engines ausführen lassen zu können.
[wikipedia, de](https://de.wikipedia.org/wiki/Business_Process_Model_and_Notation)
[wikipedia](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation)
[List of BPMN 2.0 Engines, e](https://en.wikipedia.org/wiki/List_of_BPMN_2.0_engines)
[Comparism of Tools](https://en.wikipedia.org/wiki/Comparison_of_Business_Process_Model_and_Notation_modeling_tools)

#### CRUD
Kurzform für Create, Read, Update & Delete. Beschreibt die Basis-Operationen in der Datenverarbeitung.
[wikipedia, de](https://de.wikipedia.org/wiki/CRUD)
[wikipedia](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)

#### Forrester
Ein Marktforschungsinstitut aus der IT-Branche. 
[Homepage](https://go.forrester.com/)
[LCDP Free Download](https://go.caspio.com/forrester-wave-2019-low-code-platforms-business-developers-q2)

#### Gartner
Ein Marktforschungsinstitut aus der IT-Branche. 
[Homepage](https://www.gartner.com/en)
[LCAP MQ 2019/8](https://www.gartner.com/doc/reprints?id=1-1ODHN7R7&ct=190812&st=sb)

#### IoT
Internet of Things. Grobe Umschreibung von Maschine-zu-Maschine Kommunikation via Internet-Protokolle.
[wikipedia, de](https://de.wikipedia.org/wiki/Internet_der_Dinge)
[wikipedia](https://en.wikipedia.org/wiki/Internet_of_things)

#### MDA
Model Driven Architecture. Beschreibt den Ansatz, eine Architektur immer auf Basis eines Modells zu entwickeln und zu beschreiben.
[wikipedia, de](https://de.wikipedia.org/wiki/Modellgetriebene_Architektur)
[wikipedia](https://en.wikipedia.org/wiki/Model-driven_architecture)

#### NodeRed
Ein Opensource-Tool um Events, vor allem in der IoT, miteinander zu verknüpfen.
[Homepage](https://nodered.org/) 

#### OSS as FOSS or COSS
FOSS: Free & Open Source. Freie und Quelloffene Software.
COSS: OpenSource, hier Commercial OpenSource. Immer mehr rein kommerzielle Software-Lösungen legen ihren Code offen, ohne dabei kostenlos zu sein.
OSI: OpenSource Initiative. A NGO (Non Government Organisation)  to promote OSS.
[wikipedia, de](https://de.wikipedia.org/wiki/Open_Source)
[wikipedia](https://en.wikipedia.org/wiki/Open-source_model)
[OSI Homepage](https://opensource.org/)

#### Pipes-digital
Ein weiterer Nachfolger von Yahoo! Pipes. Mit einer Community Edition (CE) in OpenSource.
[github](https://github.com/pipes-digital/pipes)
[homepage](https://www.pipes.digital/)

#### RAD
Rapid Application Development. Beschreibt ein Vorgehen durch Wiederverwendung möglichst schnell eine Lösung fertig zu stellen.
[wikipedia, de](https://de.wikipedia.org/wiki/Rapid_Application_Development)
[wikipedia](https://en.wikipedia.org/wiki/Rapid_application_development)


#### SCA
Statische Code Analyse. Mit Hilfe SCA können Fehler, Probleme, technische Schulden, Sicherheitslücken entdeckt werden.
[wikipedia, de](https://de.wikipedia.org/wiki/Statische_Code-Analyse)
[wikipedia](https://en.wikipedia.org/wiki/Static_program_analysis)

#### Sonarqube
Ein weit verbreitetes Tool aus dem Bereich der SCA,
[Homepage](https://www.sonarqube.org/) 

#### UML
Unified Modelling Language. Eine Sammlung von ~13 Diagrammen um Klassen, Komponenten, Deployments, Abläufe und mehr grafisch zu dokumentieren.Wird von der OMG (Object Managing Group) geführt.
[OMG UML Vendors](https://www.omg.org/uml-directory/) 
[wikipedia, de](https://de.wikipedia.org/wiki/Unified_Modeling_Language)
[wikipedia](https://en.wikipedia.org/wiki/Unified_Modeling_Language)

#### Unreal
Synonym für die Unreal Engine. Eine Echtzeit 3D- Rendering und Raytracing Plattform. Wird für viele AAA Computerspiele aber auch für einige VFX-Sequenzen in Filmproduktion u.a. bei HBO oder ILM benutzt.
[Homepage](https://www.unrealengine.com) 

#### VR
Virtual Reality. Beschreibt ein immersives 3D Erlebnis in der Regel mit Hilfe spezieller Brillen und Gloves.
[wikipedia, de](https://de.wikipedia.org/wiki/Virtuelle_Realit%C3%A4t)
[wikipedia](https://en.wikipedia.org/wiki/Virtual_reality)

#### VUO
VUO, vor allem aber der VUO-Editor ist ein Tool für das Mixen von Echtzeit-Multimedia Inhalten.
[Homepage](https://vuo.org/what-is-vuo)

#### WhiteSource
Ein SCA Tool, welches mithilfe verschiedener Datenbank erkennt, ob es mit OSS-Modulen bereits schon Sicherheitsproblem gab und ob bzw. wie diese gelöst wurden.
[Homepage](https://www.whitesourcesoftware.com/) 

#### Yahoo Pipes
Einer der ersten webbasierten “NoCode”-Lösungen um verschiedene Events miteinander zu verknüpfen. Gibt es so leider nicht mehr. NodeRed kommt dem am nächsten.
[wikipedia](https://en.wikipedia.org/wiki/Yahoo!_Pipes)





